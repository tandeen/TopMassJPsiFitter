#!/usr/bin/env python
#MorphFitterClass.py
###########################
''' Long description 
Class def for using moment morpf with top mass templates
Written by Tim Andeen, UT Austin, July 2018
'''

import ROOT, random 
RF=ROOT.RooFit

def Import(workspace,var):
	getattr(workspace,'import')(var)
	return 

def SaveFrameAs(frame, outputname='default.pdf',batch=False):
	c = ROOT.TCanvas()
	frame.Draw()
	c.SaveAs("%s" %outputname)
	if (not batch): ROOT.gROOT.GetListOfCanvases().Draw()
	return 

def CalcPull(input, result, error):
	return ((input-result)/error)


class morpher():

	def __init__(self, outputprefix='out', name='mymorpher', batch=False, verbose=False):
		self.name = name
		self.outputprefix = outputprefix
		self.w = ROOT.RooWorkspace('w')
		self.batch = batch
		self.verbose = verbose
		self.savefile = True
		#always reset the random seed by hand at the start
		seed = int(random.random()*1000)
		ROOT.RooRandom.randomGenerator().SetSeed(seed)


	def initializeTemplates(self, n_templates=6):
		self.n_templates = n_templates
		self.template_range = 20.
		self.m3lstep = self.template_range/(n_templates-1)
		self.sigmastep = 0.
		self.topstep = self.template_range/(n_templates-1)
		self.topmass_ref_points = ROOT.TVectorD(n_templates)
		self.m3l = ROOT.RooRealVar("m3l","m3l",60., 0., 400.)
		Import(self.w,self.m3l)
		self.topmass_ref_points = ROOT.TVectorD(int(self.n_templates))

		self.obs = ROOT.RooArgList(self.m3l)

		self.mtop = ROOT.RooRealVar("mtop","mtop",172.,170.,177.5)

		self.templates = ROOT.RooArgList()
		Import(self.w,self.templates)
		self.w.Print()
		return 

	def maketree(self, filename):
		#self.tml3 = ROOT.RooRealVar("tml3","tml3",60.,0.,400.)
		resultdata = ROOT.RooDataSet(filename,filename,ROOT.RooArgSet(self.m3l) )
		#Import(self.w,self.tml3)

		inTree = ROOT.TChain('nominal')
		inTree.Add(filename)
		mcnumEvents = inTree.GetEntries()
		print('Events selection from mc170 file: %i' % mcnumEvents)

		#for entry in inTree:
		for event in xrange(mcnumEvents):
			if (event%100000==0): print(event)
			# load the event
			nb = inTree.GetEntry(event)
			if nb<=0: print '0 bytes read for event. Error.'

			## specific to reading in Chucks files. 
			Ji = inTree.Jpsi_selected
			Jpt,Jeta,Jphi,Jm = inTree.Jpsi_Pt[Ji],inTree.Jpsi_Eta[Ji],inTree.Jpsi_Phi[Ji],inTree.Jpsi_mass[Ji]
			J4V = ROOT.TLorentzVector()
			J4V.SetPtEtaPhiM(Jpt,Jeta,Jphi,Jm)

			Li,Lpt,Leta,Lphi,Le = -1,0.,0.,0.,0.
			if inTree.muon_selected>=0:
				Li = inTree.muon_selected
				Lpt,Leta,Lphi,Le = inTree.muon_pt[Li],inTree.muon_eta[Li],inTree.muon_phi[Li],inTree.muon_E[Li]
			elif inTree.electron_selected>=0:
				Li = inTree.electron_selected
				Lpt,Leta,Lphi,Le = inTree.electron_pt[Li],inTree.electron_eta[Li],inTree.electron_phi[Li],inTree.electron_E[Li]
			else:
				print('bad!')
			L4V = ROOT.TLorentzVector()
			L4V.SetPtEtaPhiE(Lpt,Leta,Lphi,Le)
			m = (J4V+L4V).M()*.001
			self.m3l.setVal(m)
			resultdata.add( ROOT.RooArgSet(self.m3l) )

		resultdata.Print()
		return resultdata

		return

	def createTemplatesKeys(self, numkeydatasetevents):
		self.frame_templates =self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Templates'))

		for itemplate in range(self.n_templates):
			print 'makeing the %sth template.' %itemplate
			self.imass = itemplate * self.m3lstep + 50. 
			self.isigma = itemplate * self.sigmastep + 5. 

			#setup the mapping between template number and top mass
			self.topmass_ref_points[int(itemplate)] = itemplate * self.topstep + 160. 

			#make gaussian templates
			# note that mass and sigma for temapltes are constants. if you let them float the fit will try to 
			# fit thesse too (not what we want) and take forever and not converge
			self.w.factory("Gaussian::gauss_m3L{itemplate}( m3l, m3L_{itemplate}[{imass}], sigma3L_{itemplate}[{isigma}])"
															.format(itemplate=itemplate, imass=self.imass, isigma=self.isigma))  

			#now we make a pdf for each of these models
			pdf = self.w.pdf("gauss_m3L{itemplate}".format(itemplate=itemplate)) 

			#now make a low stats dataset from the gaussian
			datatemp = pdf.generate(ROOT.RooArgSet(self.m3l), numkeydatasetevents)
			#datatemp = maketree()
			datatemp.Print()
			Import(self.w,datatemp)

			f = ROOT.TFile('%s_templates%s_from_data.root' %(self.outputprefix, itemplate),'RECREATE')
			datatemp.Write()
			f.Close()


			# Adaptive kernel estimation pdf with increased bandwidth scale factor 
			# (see https://root.cern.ch/root/html/tutorials/roofit/rf707_kernelestimation.C.html)
			print 'starting =============================================================================================================================='
			kest3 = ROOT.RooKeysPdf("kernal_m3L{itemplate}".format(itemplate=itemplate), "kernal_m3L{itemplate}".format(itemplate=itemplate)
									, self.m3l, datatemp, ROOT.RooKeysPdf.MirrorAsymBoth, 2)
			print 'doneing =============================================================================================================================='

			Import(self.w,kest3)

			# make it a pdf
			pdfk = self.w.pdf("kernal_m3L{itemplate}".format(itemplate=itemplate)) 

			# and draw them altogether
			pdfk.plotOn(self.frame_templates, ROOT.RooLinkedList())

			#draw "data" and kernal:        
			temp_frame_templates =self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Templates%s' %itemplate))
			datatemp.plotOn(temp_frame_templates) 
			pdfk.plotOn(temp_frame_templates, ROOT.RooLinkedList())

			SaveFrameAs(temp_frame_templates, '%s_templates%s_from_data.pdf' %(self.outputprefix, itemplate) )


			#last we add the templates to pdfs, the list of the templates
			self.templates.add(pdfk)

		SaveFrameAs(self.frame_templates, '%s_templates_from_keys.pdf' %self.outputprefix)


	def createTemplates(self):
		self.frame_templates =self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Templates'))

		for itemplate in range(self.n_templates):
			print 'makeing the %sth template.' %itemplate
			self.imass = itemplate * self.m3lstep + 50. 
			self.isigma = itemplate * self.sigmastep + 5. 

			#setup the mapping between template number and top mass
			self.topmass_ref_points[int(itemplate)] = itemplate * self.topstep + 160. 

			#make gaussian templates
			# note that mass and sigma for temapltes are constants. if you let them float the fit will try to 
			# fit thesse too (not what we want) and take forever and not converge
			self.w.factory("Gaussian::gauss_m3L{itemplate}( m3l, m3L_{itemplate}[{imass}], sigma3L_{itemplate}[{isigma}])"
															.format(itemplate=itemplate, imass=self.imass, isigma=self.isigma))  

			#now we make a pdf for each of these models
			pdf = self.w.pdf("gauss_m3L{itemplate}".format(itemplate=itemplate)) 
			# and draw them altogether
			pdf.plotOn(self.frame_templates)

			#last we add the templates to pdfs, the list of the templates
			self.templates.add(pdf)

		SaveFrameAs(self.frame_templates, '%s_templates_from_guass.pdf' %self.outputprefix)

		return

	def readtree(self, filename, nevents=-1):

		#self.tml3 = ROOT.RooRealVar("tml3","tml3",60.,0.,400.)
		resultdata = ROOT.RooDataSet(filename,filename,ROOT.RooArgSet(self.m3l) )
		#Import(self.w,self.tml3)

		inTree = ROOT.TChain('nominal')
		inTree.Add(filename)
		mcnumEvents = inTree.GetEntries()
		print('Events selection from mc170 file: %i' % mcnumEvents)

		erange = mcnumEvents
		if nevents>0: 
			erange = nevents
		for event in xrange(erange):
			if (event%100000==0): print(event)
			# load the event
			nb = inTree.GetEntry(event)
			if nb<=0: print '0 bytes read for event. Error.'

			## specific to reading in Chucks files. 
			Ji = inTree.Jpsi_selected
			Jpt,Jeta,Jphi,Jm = inTree.Jpsi_Pt[Ji],inTree.Jpsi_Eta[Ji],inTree.Jpsi_Phi[Ji],inTree.Jpsi_mass[Ji]
			J4V = ROOT.TLorentzVector()
			J4V.SetPtEtaPhiM(Jpt,Jeta,Jphi,Jm)

			Li,Lpt,Leta,Lphi,Le = -1,0.,0.,0.,0.
			if inTree.muon_selected>=0:
				Li = inTree.muon_selected
				Lpt,Leta,Lphi,Le = inTree.muon_pt[Li],inTree.muon_eta[Li],inTree.muon_phi[Li],inTree.muon_E[Li]
			elif inTree.electron_selected>=0:
				Li = inTree.electron_selected
				Lpt,Leta,Lphi,Le = inTree.electron_pt[Li],inTree.electron_eta[Li],inTree.electron_phi[Li],inTree.electron_E[Li]
			else:
				print('bad!')
			L4V = ROOT.TLorentzVector()
			L4V.SetPtEtaPhiE(Lpt,Leta,Lphi,Le)
			m = (J4V+L4V).M()*.001
			self.m3l.setVal(m)
			resultdata.add( ROOT.RooArgSet(self.m3l) )

		resultdata.Print()
		return resultdata


	def readTemplates(self, inputfile):
		self.frame_templates =self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Templates'))
		print 'N templates : %s' %self.n_templates
		for itemplate in range(self.n_templates):
		#for itemplate in range(1):
			temp_frame_templates =self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Templates%s' %itemplate))

			print 'makeing the %sth template.' %itemplate

			''' mapping from ds to template to top mass: 
					410037.PowhegPythiaEvtGen template0 170.0 
					410038.PowhegPythiaEvtGen template1 171.5
					410000.PowhegPythiaEvtGen template2 172.5 
					410039.PowhegPythiaEvtGen template3 173.5 
					410049.PowhegPythiaEvtGen template4 175.0 
					410041.PowhegPythiaEvtGen template5 177.5 
			'''
			
			#setup the mapping between template number and top mass
			templatemappingdictNom = {'template0':170.0
								 , 'template1':171.5 
								 , 'template2':172.5 
								 , 'template3':173.5 
								 , 'template4':175.0 
								 , 'template5':177.5 }
 			templatemappingdict = {'template0':170.0
								 , 'template1':171.5 
								 , 'template2':173.5 
								 , 'template3':175.0 
								 , 'template4':177.5 }

			tname = 'template%s' %itemplate
			print '-------------------------------------------- name %s mass %s' %(tname, templatemappingdict[tname])
			self.topmass_ref_points[int(itemplate)] = templatemappingdict[tname]

			#self.topmass_ref_points[int(itemplate)] = itemplate * self.topstep + 160. 

			#open files in m3l function
			fname = inputfile + tname + '.root'

			#read data (tree prefereably) from file
			#needs to have the name like : atemplate1 atemplate2 etc for the next line to work. 
			#make a pdf for each mass point 

			#self.readtree(fname)
			datatemp = self.readtree(fname,9000) #  read in ttree from file. or histogram if necessary. perhaps allow for both
			datatemp.Print()
			Import(self.w,datatemp)

			#f = ROOT.TFile('%s_templates%s_from_data.root' %(self.outputprefix, itemplate),'recreate')
			#datatemp.Write()
			#f.Close()

			'''
			# Adaptive kernel estimation pdf with increased bandwidth scale factor 
			# (see https://root.cern.ch/root/html/tutorials/roofit/rf707_kernelestimation.C.html)
			print 'starting =============================================================================================================================='
			kest4 = ROOT.RooKeysPdf("kernal_m3L{itemplate}".format(itemplate=itemplate), "kernal_m3L{itemplate}".format(itemplate=itemplate)
									, self.m3l, datatemp, ROOT.RooKeysPdf.MirrorAsymBoth,2)
			print 'doneing =============================================================================================================================='

			Import(self.w,kest4)

			# make it a pdf
			pdfk = self.w.pdf("kernal_m3L{itemplate}".format(itemplate=itemplate)) 
			'''

			self.m3l.setBins(100)
			histtemp = datatemp.binnedClone()
			histpdftemp = ROOT.RooHistPdf("kernal_m3L{itemplate}".format(itemplate=itemplate), "kernal_m3L{itemplate}".format(itemplate=itemplate), ROOT.RooArgSet(self.m3l), histtemp, 0)
			
			Import(self.w,histpdftemp)

			pdfk = self.w.pdf("kernal_m3L{itemplate}".format(itemplate=itemplate)) 

			# and draw them altogether
			pdfk.plotOn(self.frame_templates, ROOT.RooLinkedList())
			datatemp.plotOn(temp_frame_templates) 
			pdfk.plotOn(temp_frame_templates, ROOT.RooLinkedList())

			#last we add the templates to pdfs, the list of the templates
			self.templates.add(pdfk)
			SaveFrameAs(temp_frame_templates, '%s_templates%s_from_data.pdf' %(self.outputprefix, itemplate) )

		SaveFrameAs(self.frame_templates, '%s_templates_from_keys.pdf' %self.outputprefix)

		return


	def createMorph(self):
		'''SETUP MORPHING
		      Options are: Linear  NonLinear NonLinearPosFractions NonLinearLinFractions SineLinear '''
		setting = ROOT.RooMomentMorph.Linear  
		self.mymorph2 = ROOT.RooMomentMorph('mymorph2','mymorph2', self.mtop, self.obs, self.templates, self.topmass_ref_points, setting)  
		Import(self.w,self.mymorph2)
		self.w.Print()
		return

	def checkTemplates(self):
		#make sure it worked: 
		self.frame_templates.Draw()
		self.mtop.setVal(173.12) #not exactly on a template value
		self.mymorph2.Print()
		self.mymorph2.plotOn(self.frame_templates, RF.LineColor(ROOT.kRed))
		SaveFrameAs(self.frame_templates, '%s_templates_and_test.pdf' %self.outputprefix)
		return

	def printTemplateInfo(self):
		print" ----------------------------------------------------------------------------------------------------------------"      
		print" --------------- Initialized ---------------"      
		print" n templates    = %s        |" %self.n_templates
		print" template range = %s        |" %self.template_range
		print" m3l step       = %s        |" %self.m3lstep       
		print" sigma step     = %s        |" %self.sigmastep     
		print" top step       = %s        |" %self.topstep       

		self.w.Print()
		self.topmass_ref_points.Print()
		print" ----------------------------------------------------------------------------------------------------------------"      

		return

	def createData(self, mytopmass, nevents=100):
		
		self.mtop.setVal(mytopmass) #not exactly on a template value
		
		self.d2 = self.mymorph2.generate(ROOT.RooArgSet(self.m3l), nevents)
		self.d2.Print()

		frame2 = self.m3l.frame(RF.Bins(100), RF.Range(40,90), RF.Title('Test distribution')) 
		self.d2.plotOn(frame2) 
		self.mymorph2.plotOn(frame2, RF.LineColor(ROOT.kRed));
		SaveFrameAs(frame2,'%s_dataset_testdist.pdf' %self.outputprefix)
		return

	def readData(self, nevents=100):
				
		#open file 
		#read tree and name as self.d2 
		#read from nominal sample. take first 1000 events. 
		self.d2 = self.readtree("/eos/user/t/tandeen/TopMassSamples/template3.root",nevents) 
		self.d2.Print()
		Import(self.w,self.d2)

		frame2 = self.m3l.frame(RF.Bins(400), RF.Range(0.,400.), RF.Title('Data distribution')) 
		self.d2.plotOn(frame2) 
		self.mymorph2.plotOn(frame2, RF.LineColor(ROOT.kRed));
		SaveFrameAs(frame2,'%s_dataset_datadist.pdf' %self.outputprefix)
		return


	def runFit(self):
		self.mtop.setVal(171.0) #new starting value before we try to fit

		nll = self.mymorph2.createNLL(self.d2, RF.Verbose(ROOT.kFALSE))

		frame3 = self.mtop.frame(RF.Bins(1000), RF.Range(100.,200.), RF.Title('- Log Likelihood')) 
		nll.plotOn(frame3, RF.ShiftToZero(), RF.Range(160.,180.)) 
		SaveFrameAs(frame3,'%s_nll_output.pdf' %self.outputprefix)

		if self.verbose: print "------------------------------------------ Starting fit --------------------------------------------------"
		fitresult = self.mymorph2.fitTo(self.d2, RF.Minos(ROOT.kTRUE), RF.Verbose(ROOT.kFALSE), RF.Save())
		print "Printing final floating parameters: "
		fitresult.floatParsFinal().Print("s") ;
		print fitresult.floatParsFinal().at(0).getValV(), fitresult.floatParsFinal().at(0).getAsymErrorLo(), fitresult.floatParsFinal().at(0).getAsymErrorHi()
		return fitresult


	def setNPullsNEvents(self, npulls, nevents):
		self.npulls=npulls
		self.nevents=nevents
		return

	def createGaussianDataset(self, mean, sigma):
		#always reset the random seed by hand here also
		seed = int(random.random()*1000)
		ROOT.RooRandom.randomGenerator().SetSeed(seed)
		rt = ROOT.RooRealVar("rt","rt",170.,100.,300.)
		rmean= ROOT.RooRealVar("rmean","rmean",mean)
		rsig= ROOT.RooRealVar("rsig","rsig",sigma)
		randgauss = ROOT.RooGaussian('randgauss','randgauss', rt, rmean, rsig) 
		self.rdata = randgauss.generate(ROOT.RooArgSet(rt), self.nevents)
		self.rdata.Print("v")
		#self.rdata.get(77).Print("v")
		#print 'Evt 77 %s added to 6 %s' %( self.rdata.get(77).getRealValue("rt"), (self.rdata.get(77).getRealValue("rt")+6.0) )
		return


	def generatePullPlots(self):
		self.fitinput = ROOT.RooRealVar("fitinput","fitinput",170.,100.,300.)
		Import(self.w, self.fitinput)
		self.fitresult = ROOT.RooRealVar("fitresult","fitresult",170.,100.,300.)
		Import(self.w, self.fitresult)
		self.fiterrorhi = ROOT.RooRealVar("fiterrorhi","fiterrorhi",0.,-10.,10.)
		Import(self.w, self.fiterrorhi)
		self.fiterrorlo = ROOT.RooRealVar("fiterrorlo","fiterrorlo",0.,-10.,10.)
		Import(self.w, self.fiterrorlo)
		self.fitpull = ROOT.RooRealVar("fitpull","fitpull",0.,-10.,10.)
		Import(self.w, self.fitpull)
		self.fitasym = ROOT.RooRealVar("fitasym","fitasym",0.,-10.,10.)
		Import(self.w, self.fitasym)

		self.resultdata = ROOT.RooDataSet("resultdata","resultdata",ROOT.RooArgSet(self.fitinput, self.fitresult, self.fiterrorhi, self.fiterrorlo, self.fitpull, self.fitasym) )

		for anevent in range(self.npulls):
			testmass = self.rdata.get(anevent).getRealValue("rt")
			self.fitinput.setVal(testmass)
			self.createData(testmass, self.nevents)
			fitresult = ROOT.RooFitResult( self.runFit() ) #captures the fit results as RooFitResult
			self.fitresult.setVal( fitresult.floatParsFinal().at(0).getValV() )
			self.fiterrorlo.setVal( fitresult.floatParsFinal().at(0).getAsymErrorLo() )
			self.fiterrorhi.setVal( fitresult.floatParsFinal().at(0).getAsymErrorHi() )
			pull = CalcPull( testmass, fitresult.floatParsFinal().at(0).getValV(), fitresult.floatParsFinal().at(0).getAsymErrorLo() )
			self.fitpull.setVal( pull )
			asmy = fitresult.floatParsFinal().at(0).getAsymErrorLo() + fitresult.floatParsFinal().at(0).getAsymErrorHi() 
			print 'asym is %s' %asmy
			self.fitasym.setVal( asmy )
			#add fit results to dataset
			self.resultdata.add( ROOT.RooArgSet(self.fitinput, self.fitresult, self.fiterrorhi, self.fiterrorlo, self.fitpull, self.fitasym) )
			if self.savefile: self.saveToFile()
		self.resultdata.Print()
		Import(self.w,self.resultdata)

		return 

	def plotPulls(self):

		frame_intput = self.fitinput.frame(RF.Bins(100), RF.Range(160.,180.), RF.Title('Fit Input')) 
		self.resultdata.plotOn(frame_intput, RF.Range(160.,180.)) 
		SaveFrameAs(frame_intput, outputname='fitinput.pdf')

		frame_result = self.fitresult.frame(RF.Bins(100), RF.Range(160.,180.), RF.Title('Fit Output')) 
		self.resultdata.plotOn(frame_result, RF.Range(160.,180.)) 
		SaveFrameAs(frame_result, outputname='fitoutput.pdf')

		# Gaussian for fitting pull distribution:
		pmean= ROOT.RooRealVar("pmean","pmean",0.,-10.,10.)
		psigma= ROOT.RooRealVar("psigma","psigma",1.,-10.,10.)
		pgauss = ROOT.RooGaussian('pgauss','pgauss', self.fitpull, pmean, psigma) 

		frame_pulls = self.fitpull.frame(RF.Bins(100), RF.Range(-5.,5.), RF.Title('Fit Pulls')) 
		self.resultdata.plotOn(frame_pulls, RF.Range(-5.,5.)) 
		pfitresult = pgauss.fitTo(self.resultdata, RF.Save())
		pfitresult.Print()
		pgauss.plotOn(frame_pulls, RF.LineColor(ROOT.kRed))
		SaveFrameAs(frame_pulls, outputname='fitpulls.pdf')

		frame_asym = self.fitasym.frame(RF.Bins(100), RF.Range(-5.,5.), RF.Title('Fit Error Asymmetry')) 
		self.resultdata.plotOn(frame_asym, RF.Range(-5.,5.)) 
		SaveFrameAs(frame_asym, outputname='fitasmy.pdf')

		self.w.writeToFile("%s_pullworkspace.root" %self.outputprefix) 
		return

	def saveToFile(self):
		fout = open("%s_fitoutput.txt" %self.outputprefix,"a+")
		fout.write("%s %s %s %s %s %s\n" % (self.fitinput.getValV(), self.fitresult.getValV(), self.fiterrorhi.getValV()
			, self.fiterrorlo.getValV(), self.fitpull.getValV(), self.fitasym.getValV()))
		fout.close()		
		return
