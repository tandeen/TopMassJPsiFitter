#!/bin/bash                                                                                                                                                                                                      
#helper cript for batch submission. should make this all a docker of course... 

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
export ALRB_localConfigDir=/code/localConfig
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

setupATLAS
lsetup ROOT
lsetup git
localSetupSFT --cmtConfig=x86_64-slc6-gcc62-opt releases/LCG_92/blas/20110419
localSetupSFT --cmtConfig=x86_64-slc6-gcc62-opt releases/LCG_92/lapack/3.5.0
localSetupSFT --cmtConfig=x86_64-slc6-gcc62-opt releases/LCG_92/pyanalysis/2.0
lsetup xrootd


