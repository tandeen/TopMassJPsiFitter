#!/usr/bin/env python
#pullmaker.py

###########################
''' Long description 

Written by Tim Andeen, UT Austin, July 2018
'''
#import string, getopt, sys, random, math, time, os, shutil, urllib2, pprint
#from ROOT import gROOT, TCanvas, TF1, TH1D, TFile, TPostScript, TChain, gDirectory
#from math import sqrt
import argparse
import ROOT, random
import numpy 
import glob

	 

def filldrawsave_hist(hist, array):
	#straight python + ROOT
	[hist.Fill(_) for _ in array]
	c1 = ROOT.TCanvas("c","c",800,800)
	hist.Draw()
	c1.SaveAs('%s.pdf' %hist.GetName())

def fitdrawsave_hist(array, name):
	#python + Roofit
	dvar = ROOT.RooRealVar("dvar","dvar",0.,-10.,10.)
	dset = ROOT.RooDataSet("dset","dset",ROOT.RooArgSet(dvar) )
	#[dset.add( ROOT.RooArgSet(dvar.setVal(_) ) ) for _ in array] #<- I dont think this can work here? Instead: 
	for avar in array: 
		dvar.setVal(avar)
		print avar
		dset.add( ROOT.RooArgSet(dvar) )

	#setup Gaussian, someday will take start and limits as arg
	mean= ROOT.RooRealVar("mean","mean",0.,-10.,10.)
	sigma= ROOT.RooRealVar("sigma","sigma",1.,-10.,10.)
	gauss = ROOT.RooGaussian('gauss','gauss', dvar, mean, sigma) 

	#make frame
	#frame_dvar = self.fitinput.frame(RF.Bins(100), RF.Range(160.,180.), RF.Title('Fit Input')) 

	frame_dvar = dvar.frame(ROOT.RooFit.Bins(100), ROOT.RooFit.Title( '%s' %name )) 
	#plot dataset
	dset.plotOn(frame_dvar) 
	#fit
	fitresult = gauss.fitTo(dset, ROOT.RooFit.Save())
	fitresult.Print()
	#plot fit
	gauss.plotOn(frame_dvar, ROOT.RooFit.LineColor(ROOT.kRed))

	#save it
	c1 = ROOT.TCanvas("c","c",800,800)
	frame_dvar.Draw()
	c1.SaveAs('%s_fit.pdf' %name)


def openandread(afile):
	with open(afile, 'r') as f:
		read_data = numpy.loadtxt(f)
	return read_data

def plotpulls(input):

	#read and combine files:
	dataarray = numpy.concatenate([openandread(_) for _ in glob.glob("%s_*_fitoutput.txt" %input)])
	
	#select pulls for plotting
	pulls = numpy.transpose(dataarray[:,[4]])[0]

	#make histogram for pulls
	hist = ROOT.TH1F('pulls', 'pulls', 40, -4, 4)

	#fill draw and save the histogram 
	filldrawsave_hist(hist, pulls)

	#fit and save 
	fitdrawsave_hist(pulls, 'pulls')

	return

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--input", required=True, help="input trees or histos")
	ap.add_argument("-o", "--output", required=False, help="output path")
	ap.add_argument("-ne", "--nevents", required=False, help="number events in dataset")
	ap.add_argument("-np", "--npulls", required=False, help="number of pull tests to do")

	args = vars(ap.parse_args())

	#ROOT.gROOT.SetBatch(batch)
	#gROOT.SetStyle("Plain")

	plotpulls(args["input"])


	return

###########################
if __name__ == "__main__":
    main()
#
