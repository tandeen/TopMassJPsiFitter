#!/usr/bin/env python
#pullsrunner.py
###########################
''' Long description 

Written by Tim Andeen, UT Austin, July 2018
'''
#import string, getopt, sys, random, math, time, os, shutil, urllib2, pprint
#from ROOT import gROOT, TCanvas, TF1, TH1D, TFile, TPostScript, TChain, gDirectory
#from math import sqrt
import argparse
import subprocess
import os

def runpulls(n,output):

	for irun in xrange(n):
		p = subprocess.Popen( ('%s/TopMassJPsiMorphFitter.py' %os.getcwd(), '-o', '%s_%s' %(output,irun),  '-ne', '10000', '-np', '10') )
		p.wait()
	return


def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--input", required=False, help="input trees or histos")
	ap.add_argument("-o", "--output", required=True, help="output path")
	ap.add_argument("-ne", "--nevents", required=False, help="number events in dataset")
	ap.add_argument("-np", "--npulls", required=False, help="number of pull tests to do")
	ap.add_argument("-nr", "--nruns", required=True, help="number of runs to make")

	args = vars(ap.parse_args())

	#ROOT.gROOT.SetBatch(batch)
	#gROOT.SetStyle("Plain")
	#array = numpy.random.rand(10)

	runpulls(int(args["nruns"]), args["output"])

	return

###########################
if __name__ == "__main__":
    main()
#