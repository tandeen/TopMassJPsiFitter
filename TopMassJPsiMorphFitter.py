#!/usr/bin/env python
#TopMassJPsiMorphFitter.py
###########################
''' Long description 

run 1 test with
>python TopMassJPsiMorphFitter.py -o myoutput -ne 10000 >& temp.out &

run pulls with (this is slow, so rather use the batch system)
> python TopMassJPsiMorphFitter.py -o myoutput -np 100 -ne 10000 >& temp.out &

in either case if you want to run with kernal estimation add "-k"

to read templates from file (note implies kernal esstimation)
> python TopMassJPsiMorphFitter.py -o myoutput -ne 10000 -i /eos/user/t/tandeen/TopMassSamples/  >& temp.out &

Written by Tim Andeen, UT Austin, July 2018
'''

#import string, getopt, sys, random, math, time, os, shutil, urllib2, pprint
#from ROOT import gROOT, TCanvas, TF1, TH1D, TFile, TPostScript, TChain, gDirectory
#from math import sqrt
#import numpy as np

import argparse
import ROOT
import MorphFitterClass as mfc 

batch = True
verbose = False

def runPulls( npulls, nevents, usekernal=False, ksize=1000, outputprefix='pulls'):

	M = mfc.morpher(outputprefix)
	M.initializeTemplates(5)
	if usekernal:
		M.createTemplatesKeys(ksize)
	else:
		M.createTemplates()
	#M.readTemplates()
	M.createMorph()
	M.setNPullsNEvents(npulls,nevents)
	M.createGaussianDataset(172., 2.)
	M.generatePullPlots()
	M.plotPulls()

	return

def runIt( nevents, usekernal=False, ksize=1000, outputprefix='test', inputfile=None):

	M = mfc.morpher(outputprefix)
	M.initializeTemplates(5)
	if not inputfile=='none':
		M.readTemplates(inputfile) #read dataset from ttree -> kernal
	elif usekernal:
		M.createTemplatesKeys(ksize) #analytical function -> dataset -> kernal
	else:
		M.createTemplates() # out of analytical function
	M.createMorph()
	M.printTemplateInfo()
	M.checkTemplates()
	if not inputfile=='none':
		#M.readData(1000)
		M.createData(172.331, nevents)
	else:
		M.createData(174.331, nevents)
	fitresult = ROOT.RooFitResult( M.runFit() )
	fitresult.Print("v")

	return 

def main():
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--input", required=False, help="input trees or histos",  default='none')
	ap.add_argument("-o", "--output", required=True, help="output path", default='myoutput')
	ap.add_argument("-ne", "--nevents", required=True, help="number events in dataset")
	ap.add_argument("-np", "--npulls", required=False, help="number of pull tests to do")
	ap.add_argument("-k", "--kernal", required=False, help="use kernal pdf estimation for template",type=bool, default=False)
	ap.add_argument("-nk", "--nkevts", required=False, help="number of events for kernal inputs", type=int, default=0)

	args = vars(ap.parse_args())

	ROOT.gROOT.SetBatch(batch)
    #ROOT.gROOT.SetStyle("Plain")

 	if not (args["npulls"] == None):
		runPulls( int(args["npulls"]), int(args["nevents"]), bool(args["kernal"]), int(args["nkevts"]), args["output"] )
	else:
		runIt(int(args["nevents"]), usekernal=bool(args["kernal"]), ksize=int(args["nkevts"]), outputprefix=args["output"], inputfile=args["input"])

	return

###########################
if __name__ == "__main__":
    main()
#
